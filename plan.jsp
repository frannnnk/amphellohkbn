<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%  response.setHeader("Access-Control-Allow-Origin","*");%>
{
 "items": [
   {
     "title": "1000M家居寬頻 + myTV SUPER",
     "category":"b",
     "titleImage": "https://www.franks.hk/amp/1000_mytv.jpg",
     "image": "https://www.franks.hk/amp/0_1s24monthdiscount.jpg",
     "price": "198",
     "content": ["1000M家居寬頻","myTV SUPER基本版及1個跨屏幕同時睇","現有流動通訊組合客戶登記此寬頻計劃，可獲豁免$680安裝費及額外免3個月家居寬頻月費(合約期為27個月)"],
     "extra": "可彈性選擇以優惠月費升級至 Wi-Fi管家服務連一站式家居Wi-Fi管理",
     "url": "https://www.youtube.com/channel/UCXPBsjgKKG2HqsKBhWA4uQw"
   },
   {
     "title": "Wi-Fi管家(1Gbps路由器) + 家居電話組合 + 24個月myTV SUPER基本版",
     "category":"w",
     "titleImage": "https://www.franks.hk/amp/hometel_mytv_wifi.jpg",
     "image": "https://www.franks.hk/amp/0_3HTWIFI.jpg",
     "price": "88",
     "content": ["Wi-Fi 管家 (採用Intel處理器之1Gps路由器)","家居電話 + IDD0030 (100分鐘)","24個月myTV SUPER基本版及1個跨屏幕同時睇"],
     "extra": "可彈性選擇以優惠月費升級至 Wi-Fi管家服務連一站式家居Wi-Fi管理",
     "url": "https://www.youtube.com/channel/UCXPBsjgKKG2HqsKBhWA4uQw"
   }
 ]
}
